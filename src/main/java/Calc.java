public class Calc {

    public static void main(String[] args) {
        System.out.println(dodawanie(2,3));
        System.out.println(odejmowanie(2,3));
        System.out.println(mnozenie(2,3));
        dzielenie(2,0);
    }

    public static int dodawanie(int a, int b){
        return a + b;
    }

    public static int odejmowanie(int a, int b){
        return a - b;
    }
    public static int mnozenie(int a, int b){
        return a * b;
    }

    public static void dzielenie(int a, int b){
        if (b == 0){
            System.out.println("Dzielenie przez zero!");
        }else {
            System.out.println(a/b);
        }

    }
}
